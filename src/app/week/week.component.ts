import { Component, DoCheck, OnInit, ViewContainerRef } from "@angular/core";
import { Course } from "../course";
import { CoursesService } from "../courses.service";

@Component({
  selector: "app-week",
  templateUrl: "./week.component.html",
  styleUrls: ["./week.component.scss"],
})
export class WeekComponent implements OnInit, DoCheck {
  ViewContainer: ViewContainerRef[];
  courses: Course[][] = [[], [], [], [], [], [], []];
  weekDays = [
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday",
  ];

  constructor(private CoursesService: CoursesService) {}
  ngOnInit() {
    this.courses = this.CoursesService.getWeekCourses();
  }

  ngDoCheck() {
    this.courses.forEach((coursesDay) => {
      coursesDay.sort((c1, c2) => {
        const time1 = c1.startTime;
        const time2 = c2.startTime;
        if (time1 < time2) {
          return -1;
        } else {
          return 1;
        }
      });
      coursesDay.forEach((course) => {
        course.isOverlapping = false;
      });
      coursesDay.forEach((course) => {
        coursesDay.forEach((course2) => {
          if (course.id != course2.id) {
            let fstarttime = course.startTime;
            let sstarttime = course2.startTime;
            let fendtime = course.endTime;
            let sendtime = course2.endTime;
            if (
              (fstarttime < sstarttime && fendtime > sstarttime) ||
              (sstarttime < fstarttime && sendtime > fstarttime) ||
              (sstarttime == fstarttime && sendtime == fendtime)
            ) {
              course.isOverlapping = true;
              course2.isOverlapping = true;
            }
          }
        });
      });
    });
  }

  onDeleteAppointment(course: Course) {
    this.CoursesService.removeWeekCourse(course);
    course.isOverlapping = false;
  }
}
