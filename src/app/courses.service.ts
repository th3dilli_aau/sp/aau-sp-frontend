import { Injectable } from "@angular/core";
import { Course, ICourse } from "./course";
import { HttpClient } from "@angular/common/http";
import { environment } from "../environments/environment";
import { Subject } from "rxjs";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root",
})
export class CoursesService {
  courses: Course[] = [];
  weekCourses: Course[][] = [[], [], [], [], [], [], []];
  coursesUrl = `${environment.apiurl}/coursesv2`;

  subject: Subject<Course[]>;

  constructor(private http: HttpClient) {
    this.subject = new Subject<Course[]>();
  }

  init() {
    this.fetchCourses();
  }

  fetchCourses() {
    this.http
      .get<ICourse[]>(this.coursesUrl)
      .pipe(
        map((data) => {
          return data.map(
            (c) =>
              new Course(
                c.id,
                c.type,
                c.title,
                c.professor,
                c.weekDay,
                c.startTime,
                c.endTime,
                c.sws
              )
          );
        })
      )
      .subscribe(
        (data) => {
          this.courses = data;
          this.subject.next(data);
          let loaddata = JSON.parse(
            window.localStorage.getItem("aau-semester-planner")
          );
          if (loaddata) {
            loaddata.forEach((lvnr: String) => {
              let newcourse = this.courses.find((c) => c.id === lvnr);
              if (newcourse) {
                newcourse.icon = "delete";
                this.addWeekCourse(newcourse);
              }
            });
          }
        },
        (error) => {
          console.error(error);
        }
      );
  }

  getCourses() {
    return this.subject.asObservable();
  }

  getWeekCourses() {
    return this.weekCourses;
  }
  getNum(course: Course) {
    switch (course.weekDay) {
      case "Monday":
        return 0;
      case "Tuesday":
        return 1;
      case "Wednesday":
        return 2;
      case "Thursday":
        return 3;
      case "Friday":
        return 4;
      case "Saturday":
        return 5;
      case "Sunday":
        return 6;
    }
  }

  addWeekCourse(course: Course) {
    this.weekCourses[this.getNum(course)].push(course);
  }

  removeWeekCourse(course: Course) {
    this.weekCourses[this.getNum(course)] = this.weekCourses[
      this.getNum(course)
    ].filter((coursearr) => coursearr !== course);
    const id = this.courses.findIndex((coursearr) => coursearr === course);
    this.courses[id].icon = "add";
  }
}
