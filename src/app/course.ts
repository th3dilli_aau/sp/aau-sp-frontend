export interface ICourse {
  id: string;
  type: string;
  title: string;
  professor: string;
  weekDay: string;
  startTime: number;
  endTime: number;
  sws: number;
}

export class Course {
  id: string;
  type: string;
  title: string;
  professor: string;
  weekDay: string;
  startTime: number;
  endTime: number;
  sws: number;
  icon: string;
  isOverlapping: Boolean | null;
  constructor(
    id: string,
    type: string,
    title: string,
    professor: string,
    weekDay: string,
    startTime: number,
    endTime: number,
    sws: number,
    icon?: string
  ) {
    this.id = id;
    this.type = type;
    this.title = title;
    this.professor = professor;
    this.weekDay = weekDay;
    this.startTime = startTime;
    this.endTime = endTime;
    this.sws = sws;
    this.isOverlapping = false;
    if (icon) {
      this.icon = icon;
    } else {
      this.icon = "add";
    }
  }

  getTime() {
    let timestring: string = this.weekDay + " ";
    timestring +=
      Math.floor(this.startTime / 60) === 0
        ? "00:"
        : Math.floor(this.startTime / 60).toString() + ":";
    timestring +=
      this.startTime % 60 === 0
        ? "00 - "
        : (this.startTime % 60).toString() + " - ";
    timestring +=
      Math.floor(this.endTime / 60) === 0
        ? "00:"
        : Math.floor(this.endTime / 60).toString() + ":";
    timestring +=
      this.endTime % 60 === 0 ? "00" : (this.endTime % 60).toString();

    return timestring;
  }
}
