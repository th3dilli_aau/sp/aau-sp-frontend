import {Component, Input, OnInit} from '@angular/core';
import {Course} from 'src/app/course';
import {CoursesService} from 'src/app/courses.service';

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.scss']
})
export class CourseComponent implements OnInit {

  @Input() course: Course;

  constructor(private coursesService: CoursesService) {

  }

  ngOnInit() {
  }


  buttonToggle() {
    if (this.course.icon === 'add') {
      this.course.icon = 'delete';
      this.coursesService.addWeekCourse(this.course);
    } else {
      this.course.icon = 'add';
      this.coursesService.removeWeekCourse(this.course);
    }
  }

}
