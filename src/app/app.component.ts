import { Component, OnInit } from "@angular/core";
import { CoursesService } from "./courses.service";
import { MatSnackBar } from "@angular/material/snack-bar";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent implements OnInit {
  title = "aau-semester-planner";
  constructor(
    private coursesService: CoursesService,
    private _snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.coursesService.init();
  }

  save() {
    let data = [];
    this.coursesService.weekCourses.forEach((weekcourses) => {
      weekcourses.forEach((course) => {
        data.push(course.id);
      });
    });
    window.localStorage.setItem("aau-semester-planner", JSON.stringify(data));
    this._snackBar.open("Saved!", "OK", { duration: 2000 });
  }
}
