import { Component, OnInit } from "@angular/core";
import { Course } from "../course";
import { CoursesService } from "../courses.service";

@Component({
  selector: "app-courses",
  templateUrl: "./courses.component.html",
  styleUrls: ["./courses.component.scss"],
})
export class CoursesComponent implements OnInit {
  courses: Course[];
  coursesRaw: Course[];

  constructor(private coursesService: CoursesService) {}

  ngOnInit() {
    this.coursesService.getCourses().subscribe((data) => {
      this.courses = data;
      this.coursesRaw = data;
    });
  }

  search(value: string) {
    if (value.length === 0) {
      this.courses = this.coursesRaw;
    } else if (value.length > 2) {
      this.courses = this.coursesRaw.filter((course) =>
        course.title.toLowerCase().includes(value.toLowerCase())
      );
    }
  }
}
